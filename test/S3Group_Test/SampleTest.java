package Selenumsearch;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.*;
import org.openqa.selenium.interactions.Actions;

/*****************************************************************************************************************/
/******Step1: System set up for Fire fox Drirver and navigate to www.google.com
 *     Step2 : Enter Search text in Google search "s3group.com and click on the first search from the list of searches
 *             returned and it takes to the www.s3group.com
 *     Step3 : select the ABOUT option from the S3 GROUP home website.Before spanning on the About option Tab.
 *     Step4 : Accept the s3group_accept_cookie
 *     Step5  :Using the Action tab and select the customer option which  is hidden Element and made 
 *             it visible through  JavascriptExecutor
 *     Step6 : Just checks for one header text is verifying through ASSERT command. 
 *             ******/
/****************************************************************************************************************/

public class SampleTest {
	public static void main(String[] args) throws InterruptedException {
//    System.setProperty("webdriver.gecko.driver","C:\\Users\\Prasanna\\geckodriver.exe");
	WebDriver driver = new FirefoxDriver();
	driver.navigate().to("https://www.google.com/");
	driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	
//Step 2:  Enter the text "S3group.com" in google search
/*****************************************************************************************************************/
	WebElement e = driver.findElement(By.xpath("//input[@name='q']"));
	   e.sendKeys("S3group.com");
	   e.sendKeys(Keys.ENTER);
	   driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
/*****************************************************************************************************************/
	 
	  /*******Select the first search from the Returned search results******************************************/
    //    WebElement s3wiki = driver.findElement(By.xpath(".//*[@id='rso']/div[1]/div/div/div/h3"));
          WebElement s3wiki = driver.findElement(By.className("r"));
        s3wiki.click();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        WebElement checkcookie = driver.findElement(By.xpath(".//*[@id='cookie-acceptance']/form/div/label"));
       
  /**************************************************************************************************************/
        
   // Accepting the cookie by selecting the tick box. As the Web Element cookie check box is hidden and made it visible
   // using Java script Executor.
        
        if (!(checkcookie.isSelected())){
        	 
        	JavascriptExecutor js = (JavascriptExecutor)driver;
            // customerbutton.click();
            String script1 = "arguments[0].click();";
         	js.executeScript(script1, checkcookie);
        	//checkcookie.click();
        }
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        
  /**************************************************************************************************************/
        
  // About the company TAB is clicked and which spanned the options which has "Introduction"/"Customer"/"Quality" etc.
        
     //   driver.findElement(By.xpath(".//*[@id='mw-content-text']/table[2]/tbody/tr[6]/td/a")).click();
     //   driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        WebElement f = driver.findElement(By.xpath(".//*[@id='menu-item-653']/a"));
        f.click();
        Actions  a=  new Actions(driver);
        a.moveToElement(f);

/****************************************************************************************************************/
        
  // selecting the customer  option from the "About" TAB. from the "Customer" Web Page  and verified the presence 
  //  of  "SEMICONDUCTOR SOLUTIONS". Customer web Element is hidden, Hence Javascript executor is used.
     
         
    	
        Thread.sleep(1000L);
     JavascriptExecutor js1 = (JavascriptExecutor)driver;
     WebElement customerbutton = driver.findElement(By.xpath(".//*[@id='menu-item-644']/a"));
       // customerbutton.click();
     
        String script = "arguments[0].click();";
     	js1.executeScript(script, customerbutton);
  	
        Thread.sleep(1000L);
   // To find the h2 tag for SEMICONDUCTOR SOLUTIONS
      WebElement h2element = 
          driver.findElement(By.xpath(".//*[@id='middle']/div/div[2]/div/div/div[2]/div/div/div[1]/h2"));
           
      String comptext = h2element.getText();
      System.out.println("comptext = " + comptext);
	   
	  Assert.assertTrue(comptext.contains("SEMICONDUCTOR SOLUTIONS") );
	  
	 
}
}